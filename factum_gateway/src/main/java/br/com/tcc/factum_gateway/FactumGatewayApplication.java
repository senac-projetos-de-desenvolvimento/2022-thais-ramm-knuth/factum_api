package br.com.tcc.factum_gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class FactumGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(FactumGatewayApplication.class, args);
	}

}
