package br.com.tcc.factum_auth_api.config.http;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("factum-api")
public interface TaskClient {

    @RequestMapping(method = RequestMethod.GET, value = "/lista/{id}")
    void getTasks(@PathVariable String id);
}
