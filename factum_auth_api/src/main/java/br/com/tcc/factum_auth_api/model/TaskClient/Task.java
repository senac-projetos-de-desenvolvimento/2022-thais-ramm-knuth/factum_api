package br.com.tcc.factum_auth_api.model.TaskClient;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class Task {

    public String id;

    private Category category;

    private Project project;

    public String description;

    public LocalDateTime creationDate = LocalDateTime.now();

    public Double durationTime;

}

