package br.com.tcc.factum_auth_api.config.validation;

import lombok.Getter;

/**
 * Essa classe representa um erro de validação.
 * Retornará um JSON para substituir o JSON gigante de erro do Spring.
 */
@Getter
public class FormErrorDTO {
    public String field;
    private String message;

    public FormErrorDTO(String field, String message){
        this.field = field;
        this.message = message;
    }
}

