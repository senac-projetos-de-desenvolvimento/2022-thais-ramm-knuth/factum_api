package br.com.tcc.factum_auth_api.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class HomeController {

    @GetMapping("/pessoas")
    public String home(Principal principal) {
        return "Hello, World!" + principal.getName();
    }

    @GetMapping("/user")
    public String user(Principal principal) {
        return "Hello, User: "  + principal.getName();
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/admin")
    public String admin() {
        return "Hello, Admin!";
    }

    @PreAuthorize("hasRole('LEADER')")
    @GetMapping("/leader")
    public String leader() {
        return "Hello, Leader!";
    }

}