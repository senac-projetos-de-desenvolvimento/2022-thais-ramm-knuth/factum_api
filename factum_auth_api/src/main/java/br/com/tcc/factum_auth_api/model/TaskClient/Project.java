package br.com.tcc.factum_auth_api.model.TaskClient;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Project {

    public String id;
    private String name;

}

