package br.com.tcc.factum_auth_api.controller;

import br.com.tcc.factum_auth_api.controller.dto.UserDTO;
import br.com.tcc.factum_auth_api.controller.form.UserForm;
import br.com.tcc.factum_auth_api.model.User;
import br.com.tcc.factum_auth_api.repository.GroupRepository;
import br.com.tcc.factum_auth_api.repository.UserRepository;
import br.com.tcc.factum_auth_api.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/usuarios")
public class UserController {

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @GetMapping
    public List<UserDTO> list() {
        List<User> users = userRepository.findAll();
        return UserDTO.convert(users);
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<UserDTO> detail(@PathVariable String id) {
        Optional<User> userOptional = userService.detail(id);

        return userOptional
                .map(user -> ResponseEntity
                        .ok(new UserDTO(user)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping("/name/{name}")
    public ResponseEntity<UserDTO> detailByName(@PathVariable String name) {
        Optional<User> userOptional = userService.detailByName(name);

        return userOptional
                .map(user -> ResponseEntity
                        .ok(new UserDTO(user)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    @Transactional
    public ResponseEntity<UserDTO> register(@RequestBody
                                            @Valid UserForm userForm,
                                            UriComponentsBuilder uriBuilder) {
        User user = userService.register(userForm);
        URI uri = uriBuilder
                .path("/usuarios/{id}")
                .buildAndExpand(user.getId())
                .toUri();
        return ResponseEntity.created(uri).body(new UserDTO(user));
    }

    @PutMapping("/{id}")
    @Transactional
    public ResponseEntity<UserDTO> update(@PathVariable String id,
                                          @RequestBody
                                          @Valid UserForm userForm) {

        Optional<User> userOptional = userRepository.findById(id);
        if (userOptional.isPresent()) {
            User user = userForm.update(id, userRepository, groupRepository);
            return ResponseEntity.ok(new UserDTO(user));
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}")
    @Transactional
    public ResponseEntity<?> remove(@PathVariable String id) {
        Optional<User> userOptional = userRepository.findById(id);
        if (userOptional.isPresent()) {
            userRepository.deleteById(id);
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.notFound().build();
    }
}
