package br.com.tcc.factum_auth_api.controller;

import br.com.tcc.factum_auth_api.config.security.TokenService;
import br.com.tcc.factum_auth_api.controller.dto.TokenDTO;
import br.com.tcc.factum_auth_api.controller.dto.UserDTO;
import br.com.tcc.factum_auth_api.controller.form.LoginForm;
import br.com.tcc.factum_auth_api.model.User;
import br.com.tcc.factum_auth_api.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/auth")
public class AuthenticationController {


    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private UserService userService;

    @PostMapping
    public ResponseEntity<TokenDTO> authenticate(@RequestBody @Valid LoginForm form){
        UsernamePasswordAuthenticationToken loginData = form.convert();
        try {
            Authentication authentication = authenticationManager.authenticate(loginData);
            String token = tokenService.generateToken(authentication);
            return ResponseEntity.ok(new TokenDTO(token, "Bearer"));
        } catch (AuthenticationException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @GetMapping("/me")
    public ResponseEntity<UserDTO> authenticatedMe(@RequestHeader(name = "Authorization") String token) {
        Optional<User> authenticatedMe = userService.authenticatedClient(token.substring(7));
        System.out.println(token);
        return authenticatedMe
                .map(user -> ResponseEntity
                        .ok(new UserDTO(user)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

}
