package br.com.tcc.factum_auth_api.service;

import br.com.tcc.factum_auth_api.config.security.TokenService;
import br.com.tcc.factum_auth_api.controller.form.UserForm;
import br.com.tcc.factum_auth_api.model.User;
import br.com.tcc.factum_auth_api.repository.GroupRepository;
import br.com.tcc.factum_auth_api.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    @Autowired private GroupRepository groupRepository;

    @Autowired private UserRepository userRepository;

    @Lazy
    @Autowired private PasswordEncoder passwordEncoder;

    @Autowired private TokenService tokenService;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    public User register(UserForm userForm) {
        User user = userForm.convert(groupRepository);
        user.setPass(passwordEncoder.encode(user.getPass()));
        return userRepository.save(user);
    }

    public Optional<User> detail(String id) {
        return userRepository.findById(id);
    }

    public Optional<User> detailByName(String name) {
        return userRepository.findByName(name);
    }

    public Optional<User> authenticatedClient(String token) {
        String userId = tokenService.getUserId(token);
        return userRepository.findById(userId);
    }


}