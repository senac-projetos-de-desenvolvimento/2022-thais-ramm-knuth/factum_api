package br.com.tcc.factum_auth_api.repository;

import br.com.tcc.factum_auth_api.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, String> {
        Optional<User> findByName(String name);

        Optional<User> findByEmail(String email);
}
