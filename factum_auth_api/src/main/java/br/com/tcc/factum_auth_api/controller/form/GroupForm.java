package br.com.tcc.factum_auth_api.controller.form;

import br.com.tcc.factum_auth_api.model.Group;
import br.com.tcc.factum_auth_api.repository.GroupRepository;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class GroupForm {
    @NotEmpty
    @NotNull
    private String name;

    /**
     * Acha o objeto group pelo nome
     * inserido no formulário
     * e o devolve como um
     * objeto aninhado do user.
     * Além disso, as classes form
     * validam os dados inseridos,
     * que, posteriormente,
     * são atribuídos ao seu model referente.
     */
    public Group convert(){
        return new Group(name);
    }

    public Group update(String id, GroupRepository groupRepository) {
        Group group  = groupRepository.getById(id);
        group.setName(this.name);
        return group;
    }
}
