package br.com.tcc.factum_auth_api.controller.dto;

import br.com.tcc.factum_auth_api.model.Group;
import br.com.tcc.factum_auth_api.model.TaskClient.Task;
import br.com.tcc.factum_auth_api.model.User;
import lombok.Getter;

import java.util.List;
import java.util.stream.Collectors;

@Getter
public class UserDTO {
    private final String id;
    private final String name;
    private final String email;
    private final String picture;
    private final String pass;
    private final boolean availabilityStatus;
    private final List<Group> groups;
    private final List<Task> registeredTasks;

    public UserDTO(User user) {
        this.id = user.getId();
        this.name = user.getName();
        this.email = user.getEmail();
        this.picture = user.getPicture();
        this.pass = user.getPass();
        this.availabilityStatus = user.isAvailabilityStatus();
        this.groups = user.getGroups();
        this.registeredTasks = user.getRegisteredTasks();
    }

    public static List<UserDTO> convert(List<User> users) {
        return users.stream().map(UserDTO::new).collect(Collectors.toList());
    }
}
