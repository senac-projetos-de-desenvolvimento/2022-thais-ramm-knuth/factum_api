package br.com.tcc.factum_auth_api.repository;

import br.com.tcc.factum_auth_api.model.Group;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GroupRepository extends JpaRepository<Group, String> {

    List<Group> findByName(String name);
}
