package br.com.tcc.factum_auth_api.model;

import br.com.tcc.factum_auth_api.model.TaskClient.Task;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "user")
@Getter
@Setter
public class User implements UserDetails {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    public String id;
    public String name;
    public LocalDateTime creationDate = LocalDateTime.now();
    public boolean availabilityStatus;
    @Column(unique = true)
    public String email;
    public String picture;
    public String pass;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_role",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    @JsonIgnore
    public List<Group> groups = new ArrayList<>();

    @Transient
    public List<Task> registeredTasks;

    public User() {}

    public User(String name, boolean availabilityStatus,
                String email, String picture,
                String pass, List<Group> groups) {
        this.name = name;
        this.availabilityStatus = availabilityStatus;
        this.email = email;
        this.picture = picture;
        this.pass = pass;
        this.groups = groups;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.groups;
    }

    @Override
    public String getPassword() {
        return this.pass;
    }

    @Override
    public String getUsername() {
        return this.name;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
