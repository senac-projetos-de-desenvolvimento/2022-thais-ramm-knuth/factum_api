package br.com.tcc.factum_auth_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class FactumAuthApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(FactumAuthApiApplication.class, args);
		System.out.println("RUNNING AUTH API");
	}

}
