package br.com.tcc.factum_auth_api.service;

import br.com.tcc.factum_auth_api.controller.form.GroupForm;
import br.com.tcc.factum_auth_api.model.Group;
import br.com.tcc.factum_auth_api.repository.GroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class GroupService {
    @Autowired
    private GroupRepository groupRepository;

    @Lazy

    public Group register(GroupForm groupForm) {
        Group group = groupForm.convert();
        return groupRepository.save(group);
    }

    public Optional<Group> detail(String id) {
        return groupRepository.findById(id);
    }
}
