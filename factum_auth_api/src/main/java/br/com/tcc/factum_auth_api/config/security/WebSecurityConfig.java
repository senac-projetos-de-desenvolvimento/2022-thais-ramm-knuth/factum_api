package br.com.tcc.factum_auth_api.config.security;

import br.com.tcc.factum_auth_api.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private AutenticationService autenticationService;
    @Autowired
    private TokenService tokenService;
    @Autowired
    private UserRepository userRepository;

    /**
     * Para disparar manualmente o processo de autenticação no Spring Security.
     * Anotamos o método com @Bean para injetarmos no controller.
     */
    @Bean
    @Override
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

    /**
     * Configurações de autenticação
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .userDetailsService(autenticationService)
                .passwordEncoder(new BCryptPasswordEncoder());
    }

    /**
     * Configurações de autorização
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/auth").permitAll()
                /**
                 * TODO:
                 * Refatorar nomenclatura de rotas para poder aplicar as permissões corretamente.
                 * Por hora, a permissão às rotas está send gerenciado no front end apenas.
                 */
//                                .antMatchers("/usuarios").hasRole("ADMIN")
//                                .antMatchers("/usuarios/*").hasRole("ADMIN")
//                                .antMatchers("/grupos").hasRole("ADMIN")
//                                .antMatchers("/grupos/*").hasRole("ADMIN")
//                                .antMatchers("/projetos").hasRole("LEADER")
//                                .antMatchers("/projetos/*").hasRole("LEADER")
//                                .antMatchers("/categorias").hasRole("LEADER")
//                                .antMatchers("/categorias/*").hasRole("LEADER")
                .anyRequest().authenticated()
                .and().csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and().addFilterBefore(new AuthenticationByTokenFiltering(tokenService,
                        userRepository), UsernamePasswordAuthenticationFilter.class)
                .logout().permitAll();
    }
}