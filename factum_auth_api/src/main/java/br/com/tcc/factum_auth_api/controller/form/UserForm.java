package br.com.tcc.factum_auth_api.controller.form;

import br.com.tcc.factum_auth_api.model.Group;
import br.com.tcc.factum_auth_api.model.User;
import br.com.tcc.factum_auth_api.repository.GroupRepository;
import br.com.tcc.factum_auth_api.repository.UserRepository;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
public class UserForm {

    @NotEmpty @NotNull
    private String name;

    @NotNull
    private boolean availabilityStatus;

    @NotEmpty @NotNull @Email
    @Column(unique = true)
    private String email;

    @NotEmpty @NotNull
    private String picture;

    @NotEmpty @NotNull
    private String pass;

    @NotEmpty @NotNull
    private String groupName;

    /**
     * Acha o objeto group pelo nome
     * inserido no formulário
     * e o devolve como um
     * objeto aninhado do user.
     * Além disso, as classes form
     * validam os dados inseridos,
     * que, posteriormente,
     * são atribuídos ao seu model referente.
     */
    public User convert(GroupRepository groupRepository){
        List<Group> groups = groupRepository.findByName(groupName);
        return new User(name, availabilityStatus, email, picture, pass, groups);
    }

    public User update(String id, UserRepository userRepository, GroupRepository groupRepository) {
        User user = userRepository.getById(id);
        List<Group> groups = groupRepository.findByName(this.groupName);

        user.setName(this.name);
        user.setAvailabilityStatus(this.availabilityStatus);
        user.setEmail(this.email);
        user.setPicture(this.picture);
        user.setPass(this.pass);
        user.setGroups(groups);

        return user;
    }
}
