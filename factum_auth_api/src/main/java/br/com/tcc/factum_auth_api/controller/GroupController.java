package br.com.tcc.factum_auth_api.controller;

import br.com.tcc.factum_auth_api.controller.dto.GroupDTO;
import br.com.tcc.factum_auth_api.controller.form.GroupForm;
import br.com.tcc.factum_auth_api.model.Group;
import br.com.tcc.factum_auth_api.repository.GroupRepository;
import br.com.tcc.factum_auth_api.repository.UserRepository;
import br.com.tcc.factum_auth_api.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/grupos")
public class GroupController {
    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private GroupService groupService;

    @GetMapping
    public List<GroupDTO> list() {
        List<Group> groups = groupRepository.findAll();
        return GroupDTO.convert(groups);
    }

    @GetMapping("/{id}")
    public ResponseEntity<GroupDTO> detail(@PathVariable String id) {
        Optional<Group> groupOptional = groupService.detail(id);

        return groupOptional
                .map(group -> ResponseEntity
                        .ok(new GroupDTO(group)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    @Transactional
    public ResponseEntity<GroupDTO> register(@RequestBody
                                            @Valid GroupForm groupForm,
                                            UriComponentsBuilder uriBuilder) {
        Group group = groupService.register(groupForm);
        URI uri = uriBuilder
                .path("/grupos/{id}")
                .buildAndExpand(group.getId())
                .toUri();
        return ResponseEntity.created(uri).body(new GroupDTO(group));
    }


    @DeleteMapping("/{id}")
    @Transactional
    public ResponseEntity<?> remove(@PathVariable String id) {
        Optional<Group> groupOptional = groupRepository.findById(id);
        if (groupOptional.isPresent()) {
            groupRepository.deleteById(id);
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.notFound().build();
    }
}
