package br.com.tcc.factum_auth_api.config.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.ArrayList;
import java.util.List;

@RestControllerAdvice
public class ValidationErrorHandler {
    /**
     * Para dizer pro Spring que este método deve ser chamado
     * sempre que acontecer uma exception dentro de algum controller: @ExceptionHandler
     * Como parâmetro do método, passamos o tipo de exceção que chama esse método.
     * Esse método deve ser chamado toda vez que houver uma MethodArgumentNotValidException,
     * que é o tipo de exceção lançada para erros no formulário.
     *
     * Se acontecer essa execption em qualquer RestController, ele vai cair no método @ExceptionHandler().
     */
    @Autowired
    private MessageSource messageSource;
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public List<FormErrorDTO> handle(MethodArgumentNotValidException exception){
        /**
         * Vai devolver uma lista com cada um dos erros que aconteceram dentro do objeto exception.
         */
        List<FormErrorDTO> dto = new ArrayList<>();
        List<FieldError> fieldErrors = exception.getBindingResult().getFieldErrors();
        fieldErrors.forEach(e -> {
            String msg = messageSource.getMessage(e, LocaleContextHolder.getLocale());
            FormErrorDTO erro = new FormErrorDTO(e.getField(), msg);
            dto.add(erro);
        });
        return dto;
    }
    /**
     * Tendo esse interceptador, o Spring considera que deu um erro,
     * mas que não vai devolver o código 400 para o cliente.
     * Ele vai chamar o interceptador e, aqui dentro, você faz o tratamento.
     * Ele considera que fizemos o tratamento e, por padrão, ele vai devolver 200 para o cliente.
     * Mas não queremos que ele devolva um 200 - e sim um 400 - pois o usuário errou em algum campo
     * e só trataremos a mensagem de resposta aqui.
     * @ResponseStatus() especifica o status de devolução.
     */
}

