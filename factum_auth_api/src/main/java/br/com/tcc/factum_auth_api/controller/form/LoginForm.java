package br.com.tcc.factum_auth_api.controller.form;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

@Setter
@Getter
public class LoginForm {
    private String email;
    private String pass;

    public UsernamePasswordAuthenticationToken convert() {
        return  new UsernamePasswordAuthenticationToken(email, pass);
    }
}
