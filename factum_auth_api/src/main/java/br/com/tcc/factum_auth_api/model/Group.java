package br.com.tcc.factum_auth_api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "role")
@Getter
@Setter
public class Group implements GrantedAuthority {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    public String id;
    public String name;
    public LocalDateTime creationDate = LocalDateTime.now();
    @ManyToMany(mappedBy = "groups")
    @JsonIgnore
    public List<User> users;

    public Group() {}

    public Group(String name) {
        this.name = name;
    }

    @Override
    public String getAuthority() {
        return name;
    }
}
