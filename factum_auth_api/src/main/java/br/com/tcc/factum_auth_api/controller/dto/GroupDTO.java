package br.com.tcc.factum_auth_api.controller.dto;

import br.com.tcc.factum_auth_api.model.Group;
import br.com.tcc.factum_auth_api.model.User;
import lombok.Getter;

import java.util.List;
import java.util.stream.Collectors;

@Getter
public class GroupDTO {

    private final String id;
    private final String name;
    private final List<User> users;

    public GroupDTO(Group group) {
        this.id = group.getId();
        this.name = group.getName();
        this.users = group.getUsers();
    }

    public static List<GroupDTO> convert(List<Group> groups) {
        return groups.stream().map(GroupDTO::new).collect(Collectors.toList());
    }
}
