## Factum (API)
Um sitema web para registro de horas trabalhadas dos colaboradores de uma empresa, contribuindo para
- auto-gerenciamento dos colaboradores;
- geração de informação para os empregadores;
- registro formal e transparente da carga horária e atividades dos colaboradores.

## Descrição
O Factum é um sistema web em desenvolvimento que se destina, em linhas gerais, ao controle da jornada de trabalho. A principal motivação foi desenvolver um sistema para empresas de modelo de trabalho remoto (principalmente, mas não exclusivamente) que, em primeiro lugar, ajudasse o colaborador no auto-gerenciamento da sua jornada de trabalho diária, respondendo perguntas como: _O que eu fiz ontem mesmo?_ _Quantos dias eu já passei escrevendo essa documentação?_ _Quantas horas eu já trabalhei por hoje?_; e que, em segundo lugar, gerasse informações de valor que auxiliasssem gestores nas tomadas de decisões como, por exemplo: estimativas, alocação de recurso e acompanhamento da saúde da equipe, entre outras decisões; e, por fim, em terceiro lugar, que justificasse e detalhasse, formalmente, a carga horária bem como as atividades diárias dos colaboradores, a fim de comprovar o tempo decorrido, possibilitando controle de horas extras e gerenciamento de banco de horas.

### Inspiração
Este sistema tem como referência o Toggl Track, um sistema também voltado para gerenciamento e controle de horas gastas, no entanto, o Toggl é um sistema que atende diversos públicos, por isso oferece diversas funcionalidades. Já o Factum foi planejado para o contexto de uma empresa, com menos funcionalidades(porém todas essenciais), deixando o sistema mais objtivo, intuitivo e pouco dispersivo.
Por fim, o Factum deve se destacar na interface: com o botão de pausar/continuar jornada sempre visível; campos para justificar as horas e atividades na tela inicial em um clique e, no outro, o rostinho dos colegas online!

### Backlog
Acesse o backlog do sistema aqui nas [Issues do Factum no Gitlab.](https://gitlab.com/senac-projetos-de-desenvolvimento/2022-thais-ramm-knuth/factum_doc_version_2/-/issues)

## Visual
Link para a prototipação das telas no [Figma](https://www.figma.com/file/HyYPMweS0CNjGU8ejtzJkb/tcc1?node-id=0%3A1) e repositório do [projeto cliente](https://gitlab.com/senac-projetos-de-desenvolvimento/2022-thais-ramm-knuth/factum_web).

## Instalação
### Requisitos
- JDK 17.0.3.1
- MySQL 8.0.28
- Maven 3.6.3
- Node 16.15.0
- Npm 8.10.0
> Não foram realizados testes em outras versões que não as explcitadas acima.

### Rorando a primeira vez
É possível rodar o projeto executando a classe `main` da aplicação (Ctrl+Shift+F10 no Intellij) - ou por meio do build com o plugin Spring Boot do Maven `mvn spring-boot:run`.
A ordem de execução das duas últimas(3 e 4) é indiferente, mas deve acontecer depois dos dois primerios micro serviços.

Ordem de execução dos micro serviços:
1. Eureka Server
2. Gateway
3. Factum Auth API
4. Factum API 


## Utilização
**TO DO:** _Inserir link para a documentação do POSTMAN._
## Suporte
Caso haja dúvidas ou sugestões, ficarei feliz de recebê-las em thaisknuth@gmail.com.
## Roadmap
Detalhes técnicos e de planejamento na [Wiki](https://gitlab.com/senac-projetos-de-desenvolvimento/2022-thais-ramm-knuth/factum_doc_version_2/-/wikis/home)
## Contribuição
Por hora, não é possível contribuir neste respositório.
## Autora
Thais Ramm Knuth
## License
Released under the [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0.html).
## Status do projeto
Projeto em desenvolvimento.
