package br.com.tcc.factum_api.controller.dto;

import br.com.tcc.factum_api.model.Category;
import br.com.tcc.factum_api.model.Project;
import br.com.tcc.factum_api.model.Task;
import lombok.Getter;
import org.springframework.data.domain.Page;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Getter
public class TaskDTO {

    private final String id;
    private final String description;
    private final Double durationTime;
    private final Category category;
    private final Project project;
    private final LocalDate executionDate;
    private final LocalDateTime creationDate;
    private final String userId;

    public TaskDTO(Task task) {
        this.id = task.getId();
        this.description = task.getDescription();
        this.durationTime = task.getDurationTime();
        this.category = task.getCategory();
        this.project = task.getProject();
        this.creationDate = task.getCreationDate();
        this.userId = task.getUserId();
        this.executionDate = task.getExecutionDate();
    }

    public static Page<TaskDTO> convert(Page<Task> tasks) {
        return tasks.map(TaskDTO::new);
    }
}
