package br.com.tcc.factum_api.repository;

import br.com.tcc.factum_api.model.Project;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectRepository extends JpaRepository<Project, String> {

    Project findByName(String name);
}
