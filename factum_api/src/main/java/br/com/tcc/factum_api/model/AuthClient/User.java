package br.com.tcc.factum_api.model.AuthClient;

import br.com.tcc.factum_api.model.Task;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
public class User {

    public String id;
    public String name;
    public LocalDateTime creationDate = LocalDateTime.now();
    public boolean availabilityStatus;
    public String email;
    public String picture;
    public String pass;

    public User(String id, String name, LocalDateTime creationDate,
                boolean availabilityStatus, String email, String picture, String pass,
                List<Group> groups, List<Task> registeredTasks) {
        this.id = id;
        this.name = name;
        this.creationDate = creationDate;
        this.availabilityStatus = availabilityStatus;
        this.email = email;
        this.picture = picture;
        this.pass = pass;
        this.groups = groups;
        this.registeredTasks = registeredTasks;
    }

    @JsonIgnore
    public List<Group> groups;
    public List<Task> registeredTasks;


}
