package br.com.tcc.factum_api.config.http;

import br.com.tcc.factum_api.controller.dto.LoggedUserDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Optional;

@FeignClient("factum-auth-api")
public interface AuthClient {

    @RequestMapping(method = RequestMethod.GET, value = "/auth/me")
    Optional<LoggedUserDTO> retrieveLoggedUserFromClient(@RequestHeader(name = "Authorization",
            required = false) String token);

}