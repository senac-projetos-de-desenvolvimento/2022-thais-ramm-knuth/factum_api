package br.com.tcc.factum_api.service;

import br.com.tcc.factum_api.config.http.AuthClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TokenService {

    @Autowired
    private AuthClient authClient;

    public String formatToken(String bearerToken) {
        return bearerToken.substring(7);
    }

    public boolean isTokenFormatted(String token) {
        if (token == null || token.isBlank() || !token.startsWith("Bearer ")) {
            System.out.println(token);
            System.out.println("Token é null, blank ou não formatado");
            return false;
        }
        return true;
    }



}
