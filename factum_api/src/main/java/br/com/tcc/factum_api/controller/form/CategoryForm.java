package br.com.tcc.factum_api.controller.form;

import br.com.tcc.factum_api.model.Category;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CategoryForm {

    @NotEmpty
    @NotNull
    private String name;

    public Category convert() {
        return new Category(name);
    }

}
