package br.com.tcc.factum_api.controller.form;

import br.com.tcc.factum_api.model.Project;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ProjectForm {

    @NotEmpty
    @NotNull
    private String name;

    public Project convert() {
        return new Project(name);
    }
}
