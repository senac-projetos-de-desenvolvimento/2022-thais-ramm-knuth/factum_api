package br.com.tcc.factum_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.data.web.config.EnableSpringDataWebSupport;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
// Para receber os parâmetros de ordenação e paginação diretamente nos métodos do controller
@EnableSpringDataWebSupport
@EnableCaching
public class FactumApplication {

    public static void main(String[] args) {
        SpringApplication.run(FactumApplication.class, args);
        System.out.println(">>>>>>>> RODANDO <<<<<<<<");
    }

}
