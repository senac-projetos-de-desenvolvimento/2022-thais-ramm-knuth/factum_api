package br.com.tcc.factum_api.service;

import br.com.tcc.factum_api.controller.dto.LoggedUserDTO;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TaskService {
    String id;

    public String getUserId() {
        return id;
    }

    public void getUserDataFromClient(Optional <LoggedUserDTO>loggedUserData){
        Optional<Object> loggedUserId = loggedUserData.map(loggedUserDTO -> (loggedUserDTO.id));
        String str = loggedUserId.toString();
        String sub = str.substring(9);
        this.id = sub.replace("]","");
    }
}
