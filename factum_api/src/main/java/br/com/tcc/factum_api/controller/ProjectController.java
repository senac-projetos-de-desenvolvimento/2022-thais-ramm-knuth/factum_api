package br.com.tcc.factum_api.controller;

import br.com.tcc.factum_api.controller.dto.ProjectDTO;
import br.com.tcc.factum_api.controller.form.ProjectForm;
import br.com.tcc.factum_api.model.Project;
import br.com.tcc.factum_api.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/projetos")
public class ProjectController {

    @Autowired
    private ProjectRepository projectRepository;

    @GetMapping
    @Cacheable(value = "projectsList")
    public List<ProjectDTO> list() {
        List<Project> categories = projectRepository.findAll();
        return ProjectDTO.convert(categories);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProjectDTO> detail(@PathVariable String id) {
        Optional<Project> projectOptional = projectRepository.findById(id);
        return projectOptional
                .map(project -> ResponseEntity
                        .ok(new ProjectDTO(project)))
                .orElseGet(() -> ResponseEntity
                        .notFound()
                        .build());
    }

    @GetMapping("/name/{name}")
    public ResponseEntity<ProjectDTO> detailByName(@PathVariable String name) {
        Optional<Project> projectOptional = Optional.ofNullable(projectRepository.findByName(name));

        return projectOptional
                .map(project -> ResponseEntity
                        .ok(new ProjectDTO(project)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    @Transactional
    @CacheEvict(value = "projectsList", allEntries = true)
    public ResponseEntity<ProjectDTO> register(@RequestBody
                                               @Valid ProjectForm projectForm,
                                               UriComponentsBuilder uriBuilder) {
        Project project = projectForm.convert();
        projectRepository.save(project);

        URI uri = uriBuilder
                .path("/categorias/{id}")
                .buildAndExpand(project.getId())
                .toUri();
        return ResponseEntity.created(uri).body(new ProjectDTO(project));
    }

    @DeleteMapping("/{id}")
    @Transactional
    @CacheEvict(value = "projectsList", allEntries = true)
    public ResponseEntity<?> remove(@PathVariable String id) {
        Optional<Project> projectOptional = projectRepository.findById(id);
        if (projectOptional.isPresent()) {
            projectRepository.deleteById(id);
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.notFound().build();

    }
}
