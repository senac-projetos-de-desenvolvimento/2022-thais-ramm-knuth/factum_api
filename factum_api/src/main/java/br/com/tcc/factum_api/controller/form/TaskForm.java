package br.com.tcc.factum_api.controller.form;

import br.com.tcc.factum_api.model.Category;
import br.com.tcc.factum_api.model.Project;
import br.com.tcc.factum_api.model.Task;
import br.com.tcc.factum_api.repository.CategoryRepository;
import br.com.tcc.factum_api.repository.ProjectRepository;
import br.com.tcc.factum_api.repository.TaskRepository;
import br.com.tcc.factum_api.service.TaskService;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class TaskForm {

    @NotEmpty
    @NotNull
    private String description;

    @NotNull
    @DecimalMax("24.0")
    @DecimalMin("0.0")
    private Double durationTime;

    @NotEmpty
    @NotNull
    private String categoryName;

    @NotEmpty
    @NotNull
    private String projectName;

    private String date;

    // Acha o objeto category pelo nome inserido no campo e devolve como atributo da task
    public Task convert(CategoryRepository categoryRepository,
                        ProjectRepository projectRepository,
                        TaskService taskService) {
        LocalDate executionDate = LocalDate.parse(date);
        Category category = categoryRepository.findByName(categoryName);
        Project project = projectRepository.findByName(projectName);
        String userId = taskService.getUserId();
        System.out.println("EXECUTION DATE >>>>>"+executionDate);
        return new Task(category, project, description, durationTime, userId, executionDate);
    }

    public Task update(String id,
                       TaskRepository taskRepository,
                       CategoryRepository categoryRepository,
                       ProjectRepository projectRepository) {

        Task task = taskRepository.getById(id);
        Category category = categoryRepository.findByName(this.categoryName);
        Project project = projectRepository.findByName(this.projectName);

        task.setDescription(this.description);
        task.setDurationTime(this.durationTime);
        task.setCategory(category);
        task.setProject(project);
        return task;
    }
}
