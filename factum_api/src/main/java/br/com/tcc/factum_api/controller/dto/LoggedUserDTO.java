package br.com.tcc.factum_api.controller.dto;

import br.com.tcc.factum_api.model.AuthClient.Group;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class LoggedUserDTO {
    public String id;
    public String name;
    public LocalDateTime creationDate = LocalDateTime.now();
    public boolean availabilityStatus;
    public String email;
    public String picture;
    public String pass;
    public List<Group> groups = new ArrayList<>();

    public LoggedUserDTO(String id, String name, LocalDateTime creationDate,
                         boolean availabilityStatus, String email, String picture,
                         String pass, List<Group> groups) {
        this.id = id;
        this.name = name;
        this.creationDate = creationDate;
        this.availabilityStatus = availabilityStatus;
        this.email = email;
        this.picture = picture;
        this.pass = pass;
        this.groups = groups;
    }
}
