package br.com.tcc.factum_api.repository;

import br.com.tcc.factum_api.model.Task;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TaskRepository extends JpaRepository<Task, String> {

    List<Task> findByUserId(String userId, Pageable pageable);

}
