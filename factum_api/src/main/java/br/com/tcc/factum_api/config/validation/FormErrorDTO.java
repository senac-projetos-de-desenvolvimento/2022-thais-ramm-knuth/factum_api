package br.com.tcc.factum_api.config.validation;

import lombok.Getter;
import lombok.Setter;

/**
 * Essa classe representa um erro de validação.
 * Retornará um JSON para substituir o JSON gigante de erro do Spring.
 */
@Getter
public class FormErrorDTO {
    public String field;
    private String message;

    public FormErrorDTO(String field, String message){
        this.field = field;
        this.message = message;
    }
}
