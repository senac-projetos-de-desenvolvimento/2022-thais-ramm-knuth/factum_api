package br.com.tcc.factum_api.repository;

import br.com.tcc.factum_api.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, String> {

    Category findByName(String name);

}
