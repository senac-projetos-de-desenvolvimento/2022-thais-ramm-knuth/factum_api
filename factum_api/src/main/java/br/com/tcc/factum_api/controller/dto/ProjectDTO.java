package br.com.tcc.factum_api.controller.dto;

import br.com.tcc.factum_api.model.Project;
import lombok.Getter;

import java.util.List;
import java.util.stream.Collectors;

@Getter
public class ProjectDTO {
    private final String id;
    private final String name;

    public ProjectDTO(Project project) {
        this.id = project.getId();
        this.name = project.getName();
    }

    public static List<ProjectDTO> convert(List<Project> projects) {
        return projects.stream().map(ProjectDTO::new).collect(Collectors.toList());
    }
}
