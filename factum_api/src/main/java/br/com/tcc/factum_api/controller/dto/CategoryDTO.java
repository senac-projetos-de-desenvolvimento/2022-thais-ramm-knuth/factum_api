package br.com.tcc.factum_api.controller.dto;

import br.com.tcc.factum_api.model.Category;
import lombok.Getter;

import java.util.List;
import java.util.stream.Collectors;

@Getter
public class CategoryDTO {
    private final String id;
    private final String name;

    public CategoryDTO(Category category) {
        this.id = category.getId();
        this.name = category.getName();
    }

    public static List<CategoryDTO> convert(List<Category> categories) {
        return categories.stream().map(CategoryDTO::new).collect(Collectors.toList());
    }
}
