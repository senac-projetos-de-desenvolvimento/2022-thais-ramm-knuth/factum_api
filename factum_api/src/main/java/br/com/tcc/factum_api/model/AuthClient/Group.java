package br.com.tcc.factum_api.model.AuthClient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
public class Group {

    public String id;
    public String name;
    public LocalDateTime creationDate = LocalDateTime.now();
    @JsonIgnore
    public List<User> users;

}
