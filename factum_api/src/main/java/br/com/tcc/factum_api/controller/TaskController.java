package br.com.tcc.factum_api.controller;

import br.com.tcc.factum_api.config.http.AuthClient;
import br.com.tcc.factum_api.controller.dto.LoggedUserDTO;
import br.com.tcc.factum_api.controller.dto.TaskDTO;
import br.com.tcc.factum_api.controller.form.TaskForm;
import br.com.tcc.factum_api.model.Task;
import br.com.tcc.factum_api.repository.CategoryRepository;
import br.com.tcc.factum_api.repository.ProjectRepository;
import br.com.tcc.factum_api.repository.TaskRepository;
import br.com.tcc.factum_api.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/lista")
public class TaskController {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private TaskService taskService;

    @Autowired
    private AuthClient authClient;

    @GetMapping("/usuario")
    public ResponseEntity<LoggedUserDTO> loggedUserClient(HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        Optional<LoggedUserDTO> loggedUserData =
                authClient.retrieveLoggedUserFromClient(token);
        taskService.getUserDataFromClient(loggedUserData);
        return loggedUserData.map(user -> ResponseEntity
                        .ok(new LoggedUserDTO(
                                user.id,
                                user.name,
                                user.creationDate,
                                user.availabilityStatus,
                                user.email,
                                user.picture,
                                user.pass,
                                user.groups)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }


    @GetMapping
    public Page<TaskDTO> list(@PageableDefault(
            sort = "creationDate",
            direction = Sort.Direction.DESC,
            page = 0, size = 7) Pageable pagination) {

        Page<Task> tasks = taskRepository.findAll(pagination);
        return TaskDTO.convert(tasks);
    }

    @GetMapping("/usuario/{id}")
    public Page<TaskDTO> listUserTasks(@PageableDefault(
            sort = "creationDate",
            direction = Sort.Direction.DESC,
            page = 0, size = 7) Pageable pagination,
                                       @PathVariable String id) {
        List<Task> tasks = taskRepository.findByUserId(id, pagination);
        Page<Task> userTaskPage = new PageImpl<>(tasks);
        return TaskDTO.convert(userTaskPage);
    }

    @GetMapping("/{id}")
    public ResponseEntity<TaskDTO> detail(@PathVariable String id) {
        Optional<Task> taskOptional = taskRepository.findById(id);
        return taskOptional.map(task -> ResponseEntity
                        .ok(new TaskDTO(task)))
                .orElseGet(() -> ResponseEntity
                        .notFound()
                        .build());
    }

    /***
     * {@code @Transactional} para commitar no banco transações de escrita de dados
     */
    @PostMapping
    @Transactional
    public ResponseEntity<TaskDTO> register(@RequestBody
                                            @Valid TaskForm taskForm,
                                            UriComponentsBuilder uriBuilder) {
        Task task = taskForm
                .convert(categoryRepository, projectRepository, taskService);
        taskRepository.save(task);

        URI uri = uriBuilder
                .path("/lista/{id}")
                .buildAndExpand(task.getId())
                .toUri();
        return ResponseEntity.created(uri).body(new TaskDTO(task));
    }

    @PutMapping("/{id}")
    @Transactional
    public ResponseEntity<TaskDTO> update(@PathVariable String id,
                                          @RequestBody
                                          @Valid TaskForm taskForm) {
        Optional<Task> taskOptional = taskRepository.findById(id);
        if (taskOptional.isPresent()) {
            Task task = taskForm.update(id, taskRepository, categoryRepository,
                    projectRepository);
            return ResponseEntity.ok(new TaskDTO(task));
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}")
    @Transactional
    public ResponseEntity<?> remove(@PathVariable String id) {
        Optional<Task> taskOptional = taskRepository.findById(id);
        if (taskOptional.isPresent()) {
            taskRepository.deleteById(id);
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.notFound().build();
    }
}


