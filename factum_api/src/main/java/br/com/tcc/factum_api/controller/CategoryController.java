package br.com.tcc.factum_api.controller;

import br.com.tcc.factum_api.controller.dto.CategoryDTO;
import br.com.tcc.factum_api.controller.dto.ProjectDTO;
import br.com.tcc.factum_api.controller.form.CategoryForm;
import br.com.tcc.factum_api.model.Category;
import br.com.tcc.factum_api.model.Project;
import br.com.tcc.factum_api.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/categorias")
public class CategoryController {

    @Autowired
    private CategoryRepository categoryRepository;

    @GetMapping
    @Cacheable(value = "categoriesList")
    public List<CategoryDTO> list() {
        List<Category> categories = categoryRepository.findAll();
        return CategoryDTO.convert(categories);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CategoryDTO> detail(@PathVariable String id) {
        Optional<Category> categoryOptional = categoryRepository.findById(id);
        return categoryOptional
                .map(category -> ResponseEntity
                        .ok(new CategoryDTO(category)))
                .orElseGet(() -> ResponseEntity
                        .notFound()
                        .build());
    }

    @GetMapping("/name/{name}")
    public ResponseEntity<CategoryDTO> detailByName(@PathVariable String name) {
        Optional<Category> categoryOptional = Optional.ofNullable(categoryRepository.findByName(name));

        return categoryOptional
                .map(category -> ResponseEntity
                        .ok(new CategoryDTO(category)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    @Transactional
    @CacheEvict(value = "categoriesList", allEntries = true)
    public ResponseEntity<CategoryDTO> register(@RequestBody
                                                @Valid CategoryForm categoryForm,
                                                UriComponentsBuilder uriBuilder) {
        Category category = categoryForm.convert();
        categoryRepository.save(category);

        URI uri = uriBuilder
                .path("/categorias/{id}")
                .buildAndExpand(category.getId())
                .toUri();
        return ResponseEntity.created(uri).body(new CategoryDTO(category));
    }

    @DeleteMapping("/{id}")
    @Transactional
    @CacheEvict(value = "categoriesList", allEntries = true)
    public ResponseEntity<?> remove(@PathVariable String id) {
        Optional<Category> categoryOptional = categoryRepository.findById(id);
        if (categoryOptional.isPresent()) {
            categoryRepository.deleteById(id);
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.notFound().build();

    }
}

