package br.com.tcc.factum_api.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Table(name = "task")
@Getter
@Setter
public class Task {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    public String id;

    @ManyToOne
    private Category category;

    @ManyToOne
    private Project project;

    public String description;

    public LocalDateTime creationDate = LocalDateTime.now();

    public LocalDate executionDate;

    public Double durationTime;

    public String userId;

    public Task() {
    }

    public Task(Category category, Project project, String description,
                Double durationTime, String userId, LocalDate executionDate) {
        this.category = category;
        this.project = project;
        this.description = description;
        this.durationTime = durationTime;
        this.userId = userId;
        this.executionDate = executionDate;
    }

}
